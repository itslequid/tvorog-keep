const strings = {
  done: 'Готово',
  pinAria: 'Открепить',
  selectAria: 'Выбрать',
  takeANote: 'Напишите что-нибудь... Например,про творожок.',
  title: 'Заголовок',
  pinned: 'Закреплено',
  others: 'Остальные',
};

export default strings;
